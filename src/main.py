import hug
import server_state
import telegram_client
from telethon.errors import (PhoneNumberInvalidError, SessionPasswordNeededError)
from toolz import curry

from random import choice
from string import ascii_letters


@hug.post("/send_code")
def send_code_action(phone_number):
    random_uuid = lambda length: ''.join(choice(ascii_letters) for i in range(length))

    client = telegram_client.create(phone_number)

    if client.is_user_authorized():
        return {"status": "failed", "msg": "already_logged_in"}
    else:
        try:
            client.send_code_request(phone_number)
            token = random_uuid(24)

            server_state.token_to_client[token] = client
            server_state.add_token(token, phone_number)

            return {"status": "success", "token": token}
        except PhoneNumberInvalidError:
            return {"status": "failed", "msg": "phone_number_invalid"}


@hug.post("/login")
def login(token, code):
    @curry
    def attempt_login(code, client, phone):
        try:
            client.sign_in(phone, code)
            return {"status": "success"}
        except SessionPasswordNeededError:
            return {"status": "failed", "msg": "password_required"}

    telegram_client.with_token(token, attempt_login(code))


@hug.post("/login_password")
def login_password(token, password):
    telegram_client.with_token(token,
            lambda client, _: client.sign_in(password=password),
            error_message="password_failed")


@hug.post("/send_message")
def send_message(token, name, message):
    return telegram_client.with_entity(token, name,
            lambda client, _, entity: client.send_message(entity, message))


@hug.get("/get_chats")
def get_chats(token):
    return telegram_client.with_token(token,
            lambda client, _: [x.name for x in client.iter_dialogs()])


@hug.get("/get_chat")
def get_chat(token, entity):
    name = lambda s: (s.first_name + (" " + s.last_name if s.last_name else "")).strip()

    return telegram_client.with_token(token, lambda client, _: [{
        "name": name(message.sender),
        "message": message.text
    } for message in client.iter_messages(entity)])


@hug.post("/logout")
def logout(token):
    telegram_client.with_token(token, lambda c: c.logout())


# TODO: Changed `status failed` to `{ "errors": { "name": "stuff" } }
# @hug.post("/relogin")
# def relogin(token, phone):
#     try:
#         if server.state.token_to_phone[token] == phone:
#             server.state.token_to_client[token] = telegram_client(phone)
#             return {"status": "success"}
#         else:
#             return {"status": "failed", "msg": "phone_invalid"}
#     except:
#         return {"status": "failed", "msg": "token_invalid"}
