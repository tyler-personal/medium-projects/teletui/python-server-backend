from telethon.sync import TelegramClient

import server


def create(phone, token=None):
    client = TelegramClient(phone, server.state.api_id, server.state.api_hash)

    if token is not None:
        server.state.token_to_client[token] = client

    if not client.is_connected():
        client.connect()

    return client


def with_token(token, f, phone=None, error_message: str = None):
    try:
        phone = server.state.token_to_phone[token] if phone is None else phone

        try:
            client = server.state.token_to_client[token]
            note = "Found old Telegram Client"
        except KeyError:
            client = create(phone, token)
            note = "Created new Telegram Client"

        try:
            result = f(client, phone)
            data = result if type(result) == str else "N.A"
            return {"status": "success", "data": data, "note": note}
        except Exception as e:
            error = str(e) if error_message is None else error_message
    except KeyError:
        error = "token_missing_phone_number"

    return {"status": "failed", "msg": error}


def with_entity(token, name, f):
    def run(client, phone):
        # TODO: Add some code for similar names, currently this just grabbed the first found name
        entities = [dialog for dialog in client.iter_dialogs() if dialog.name == name]
        entity = entities[0]
        f(client, phone, entity)

    return with_token(token, run, error_message="entity_not_found")
