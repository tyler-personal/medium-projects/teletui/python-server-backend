from collections import namedtuple
from dataclasses import dataclass
from telethon.sync import TelegramClient
from typing import NamedTuple, Dict

import os
import pickle


__create_tokens_file_if_missing()

api_id, api_hash = [x.strip() for x in open('data').readlines()]
token_to_phone: Dict[str, str] = __read_tokens_file()
token_to_client: Dict[str, TelegramClient] = {}


def add_token(token: str, phone: str):
    token_to_phone = __read_tokens_file()
    token_to_phone[token] = phone

    __write_tokens_file(token_to_phone)
    state.token_to_phone = token_to_phone


def __create_tokens_file_if_missing():
    if not os.path.exists("tokens"):
        with open("tokens", "wb") as file:
            pickle.dump({}, file)


def __read_tokens_file() -> Dict[str, str]:
    with open("tokens", "rb") as f:
        return pickle.load(f)


def __write_tokens_file(token_to_phone: Dict[str, str]):
    with open("tokens", "wb") as f:
        pickle.dump(token_to_phone, f)
